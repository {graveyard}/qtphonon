TEMPLATE=subdirs
SUBDIRS=\
            mediaobject \
            mediaobject_wince_ds9 \

# This is for Windows CE only (we test the second phonon backend ds9 here)
!wince*: SUBDIRS -= mediaobject_wince_ds9
