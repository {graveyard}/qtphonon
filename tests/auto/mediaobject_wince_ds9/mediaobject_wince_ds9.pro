CONFIG += testcase
TARGET = tst_mediaobject_wince_ds9
QT += testlib phonon

SOURCES += ../mediaobject/tst_mediaobject.cpp
HEADERS += ../mediaobject/qtesthelper.h
RESOURCES += ../mediaobject/mediaobject.qrc
DEPLOYMENT_PLUGIN += phonon_ds9
DEFINES += tst_MediaObject=tst_MediaObject_ds9

