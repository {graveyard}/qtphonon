############################################################
# Project file for autotest for file mediaobject.h
############################################################

CONFIG += testcase
TARGET = tst_mediaobject
QT += widgets testlib
contains(QT_CONFIG, phonon):QT += phonon
SOURCES += tst_mediaobject.cpp
HEADERS += qtesthelper.h
RESOURCES += mediaobject.qrc

CONFIG += insignificant_test    # QTBUG-19539

wince*{
  DEPLOYMENT_PLUGIN += phonon_waveout
  DEFINES += tst_MediaObject=tst_MediaObject_waveout
}

symbian:{
   addFiles.files = media/test.sdp
   addFiles.path = media
   DEPLOYMENT += addFiles
   LIBS += -lCommDb -lconnmon
   TARGET.CAPABILITY += "NetworkServices"
}
