QT      += phonon widgets

HEADERS += window.h
SOURCES += window.cpp \
           main.cpp

# install
target.path = $$[QT_INSTALL_EXAMPLES]/phonon/capabilities
sources.files = $$SOURCES $$HEADERS capabilities.pro
sources.path = $$[QT_INSTALL_EXAMPLES]/phonon/capabilities
INSTALLS += target sources

wince*{
    DEPLOYMENT_PLUGIN += phonon_ds9 phonon_waveout
}

symbian {
    TARGET.UID3 = 0xA000CF69
    CONFIG += qt_example
}

maemo5: CONFIG += qt_example

simulator: warning(This example might not fully work on Simulator platform)
