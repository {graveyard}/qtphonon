QT += phonon widgets

FORMS += settings.ui
RESOURCES += mediaplayer.qrc

!win32:CONFIG += CONSOLE

SOURCES += main.cpp mediaplayer.cpp
HEADERS += mediaplayer.h

target.path = $$[QT_INSTALL_EXAMPLES]/phonon/qmediaplayer
sources.files = $$SOURCES $$HEADERS $$FORMS $$RESOURCES *.pro *.html *.doc images
sources.path = $$[QT_INSTALL_EXAMPLES]/phonon/qmediaplayer
INSTALLS += target sources

wince*{
DEPLOYMENT_PLUGIN += phonon_ds9 phonon_waveout
}

symbian {
    TARGET.UID3 = 0xA000C613

     addFiles.files = ../embedded/desktopservices/data/sax.mp3
     addFiles.path = /data/sounds/
     DEPLOYMENT += addFiles

    CONFIG += qt_demo

	LIBS += -lcommdb

    TARGET.CAPABILITY="NetworkServices"
}
